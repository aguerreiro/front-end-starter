# [EN] Frontend boilerplate 

Using Gulp 4 to automate the different tasks.
- Compile SCSS to CSS (autoprefixer and minify the code)
- Compile TypeScript to JavaScript (minifying)
- Includes a CSS normalizer (normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css)
- Includes a personal SCSS grid system ("row", "col-sm-12", "col-md-6",...)
- Browsersync and reload to refresh the project when a change is done in the code



## Credits
- Grafikart [french] :  https://www.youtube.com/watch?v=CTY-Q6DyoBs
- Coder Coder [english] :  https://www.youtube.com/watch?v=q0E1hbcj-NI


